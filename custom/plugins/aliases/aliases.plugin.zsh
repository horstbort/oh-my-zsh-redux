alias dfh='df -h -x tmpfs -x devtmpfs -x squashfs'
alias vim='vim -p'
alias nvim='nvim -p'
alias sudo='sudo '
