# Path to your oh-my-zsh installation.


export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#ZSH_THEME="blinks"

if [[ -f ~/.zshrc.local ]]
then
    source ~/.zshrc.local
fi

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable auto-setting terminal title.
if [[ "$TERM" == screen* ]]; then
    DISABLE_AUTO_TITLE="true"
fi

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  zsh-completions aliases colored-man-pages command-not-found common-aliases fd pip ripgrep rust zsh-256color safe-paste virtualenv poetry
)

source $ZSH/oh-my-zsh.sh



[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"    history-beginning-search-backward
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}"  history-beginning-search-forward


setopt interactivecomments # pound sign in interactive prompt
unsetopt sharehistory
zstyle ':completion:*' special-dirs true

# Better SSH/Rsync/SCP Autocomplete
zstyle ':completion:*:(scp|rsync):*' tag-order ' hosts:-ipaddr:ip\ address hosts:-host:host files'
zstyle ':completion:*:(ssh|scp|rsync):*:hosts-host' ignored-patterns '*(.|:)*' loopback ip6-loopback localhost ip6-localhost broadcasthost
zstyle ':completion:*:(ssh|scp|rsync):*:hosts-ipaddr' ignored-patterns '^(<->.<->.<->.<->|(|::)([[:xdigit:].]##:(#c,2))##(|%*))' '127.0.0.<->' '255.255.255.255' '::1' 'fe80::*'
zstyle -s ':completion:*:hosts' hosts _ssh_config
[[ -r ~/.ssh/config ]] && _ssh_config+=($(cat ~/.ssh/config | sed -ne 's/Host[=\t ]//p'))
zstyle ':completion:*:hosts' hosts $_ssh_config

bindkey '^[p' history-beginning-search-backward


if [ -f /usr/share/doc/fzf/examples/key-bindings.zsh ]
then
    source /usr/share/doc/fzf/examples/key-bindings.zsh
    source /usr/share/doc/fzf/examples/completion.zsh
else
    [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
fi

# fzf's command
export FZF_DEFAULT_COMMAND="fd --hidden --exclude .git"
# CTRL-T's command
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND --type f"
# ALT-C's command
export FZF_ALT_C_COMMAND="$FZF_DEFAULT_COMMAND --type d"

export FZF_DEFAULT_OPTS="
--height=80%
--multi
--preview-window=:hidden
--preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'
--color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
--bind '?:toggle-preview'
--bind 'ctrl-a:select-all'
--bind 'ctrl-e:execute(echo {+} | xargs -o vim)'
"

eval "$(atuin init --disable-up-arrow zsh)"
eval "$(atuin gen-completions --shell zsh)"
if (( $+commands[register-python-argcomplete] ))
then
    eval "$(register-python-argcomplete pipx)"
elif (( $+commands[register-python-argcomplete3] ))
then
    eval "$(register-python-argcomplete3 pipx)"
fi

if (( $+commands[zoxide] ))
then
    eval "$(zoxide init zsh)"
fi
eval "$(starship init zsh)"
